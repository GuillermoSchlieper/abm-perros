<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ABM de perros - Alta</title>
</head>
<body>
  <main>
    <h2>Alta de perro</h2>
    <form action="perros/alta.php" method="POST" class="">
      <label for="nombre">Nombre</label><br>
      <input type="text" maxlength="20" placeholder="Juan" name="nombre" required><br>

      <label for="raza">Raza</label><br>
      <input type="text" maxlength="25" placeholder="Doberman" name="raza"><br>

      <label for="edad">Edad</label><br>
      <input type="number" min="0" max="20" placeholder="5" name="edad" required><br>

      <label for="tamano">Tamaño</label><br>
      <input type="number" min="0" max="100" placeholder="Tamaño (en cm.)" name="tamano"><br>

      Sexo:<br>
      <input type="radio" name="sexo" value="M" required>
      <label for="M">Macho</label><br>
      <input type="radio" name="sexo" value="H">
      <label for="H">Hembra</label><br>

      <label for="macho">Ciudad</label><br>
      <input type="text" maxlength="25" placeholder="Ciudad" name="ciudad" required><br>

      <label for="macho">Descripcion</label><br>
      <textarea maxlength="100" placeholder="Descripcion" name="descripcion" required></textarea><br>

      <button type="submit">Dar de alta</button>
    </form>
  </main>
  <br><br>
  <a href="./index.php">< Volver</a>
</body>
</html>
