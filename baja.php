<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ABM de perros - Baja</title>
</head>
<body>
  <main>
    <h2>Baja de perro</h2>
    <form action="perros/baja.php" method="POST" class="">
      <label for="id">Ingrese ID del perro:</label>
      <input type="number" min="0" placeholder="0" name="id" required><br>

      <button type="submit">Dar de baja</button>
    </form>
  </main>
  <br><br>
  <a href="./index.php">< Volver</a>
</body>
</html>
