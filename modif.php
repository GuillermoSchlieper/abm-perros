<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ABM de perros - Modificación</title>
</head>
<body>
  <main>
    <h2>Actualizar la información del perro</h2>
    <form action="modif-form.php" method="POST" class="">
      <label for="id">Ingrese ID del perro:</label>
      <input type="number" min="0" placeholder="0" name="id" required autofocus><br>

      <button type="submit">Buscar...</button>
    </form>
  </main>
  <br><br>
  <a href="./index.php">< Volver</a>
</body>
</html>
